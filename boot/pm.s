bits 16
pmenter:
  cli
  lgdt [GDT_DESC]

  ;set protection enable bit in cr0
  mov eax, cr0
  or al, 1
  mov cr0, eax

  jmp CODESEG:pminit            ;far jump to 32-bit code

bits 32
pminit:
  ;cs is set from far jump, so just set remaining segment registers
  mov ax, DATASEG
  mov ds, ax
  mov es, ax
  mov fs, ax
  mov gs, ax
  mov ss, ax

  ;relocate stack
  mov ebp, 0x90000
  mov esp, ebp

  call start32
