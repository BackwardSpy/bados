diskload:                       ;load al sectors from dl to es:bx
  pusha

  push ax

  mov ah, 2                     ;int 0x13 read
  mov cx, 2                     ;sector (cl) and cylinder (ch)
  mov dh, 0                     ;head number

  int 0x13
  jc .disk_error                ;carry bit indicates error

  pop bx                        ;requested # of sectors to read
  cmp al, bl                    ;bios put actual # of sectors into al
  jne .sector_error

  popa
  ret

.disk_error:
  mov si, DISK_ERROR_MSG
  call puts
  mov dh, ah                    ;dl already has drive number, put error code into dh
  call putx
  call putnl

  jmp .halt

.sector_error:
  mov si, SECTOR_ERROR_MSG
  call puts
  call putnl

.halt:
  hlt
  jmp .halt


DISK_ERROR_MSG:
  db 'error reading disk. error code: ', 0

SECTOR_ERROR_MSG:
  db 'error reading sectors', 0
