org 0x7c00
bits 16

  KERNEL_ADDR equ 0x1000        ;where the kernel should live in memory

  jmp 0x0000:start16            ;ensure segment regs are zero

start16:
  mov bp, 0x9000                ;stack base
  mov sp, bp

  mov si, BOOT_MSG
  call puts
  call putnl

  ;load kernel into memory at KERNEL_ADDR
  mov al, 16
  mov bx, KERNEL_ADDR
  call diskload

  ;enter pm and jump to start32
  call pmenter

  mov si, HALT_MSG
  call puts
  call putnl
.halt:
  hlt
  jmp .halt

%include "boot/puts.s"
%include "boot/diskload.s"
%include "boot/gdt.s"
%include "boot/pm.s"

bits 32
start32:
  mov word [0xb8000], 0x1e4b    ;yellow K on blue means we're in protected mode
  call KERNEL_ADDR              ;jump into the kernel

.halt:
  hlt
  jmp .halt

  BOOT_MSG db "in 16-bit real mode. loading kernel then switching to 32-bit protected mode...", 0
  HALT_MSG db "past kernel. halting!", 0

%if ($ - $$) > 510
%fatal boot sector too large
%endif

  times 510 - ($ - $$) db 0
  dw 0xaa55
