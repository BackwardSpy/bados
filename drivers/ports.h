#ifndef PORTS_H
#define PORTS_H

#include <stdint.h>

uint8_t in_b(uint16_t port);
void out_b(uint16_t port, uint8_t data);
uint16_t in_w(uint16_t port);
void out_w(uint16_t port, uint16_t data);

#endif
