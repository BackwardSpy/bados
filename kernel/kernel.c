#include <stdint.h>
#include <stddef.h>

#include "../drivers/ports.h"
#include "../drivers/video.h"

// default colour
const uint8_t DC = 0x9b;

void put_colour_grid(void) {
  puts("   ", DC);
  for (uint8_t col = 0x0; col < 0xf; col++) {
    putx_n(col, DC);
    putc(' ', DC);
  }
  putx_n(0xf, DC);
  puts("\n 0", DC);
  for (uint8_t col = 0x00; col < 0xff; col++) {
    putx_b(col, col);
    if ((col & 0xf) == 0xf) {
      puts("\n ", DC);
      putx_n(((col & 0xf0) >> 4) + 1, DC);
    }
  }
  putx_b(0xff, 0xff);
}

size_t strlen(const char *str) {
  if (str == 0 || *str == 0) return 0;
  size_t n = 0;
  while (str[n++]);
  return n;
}

void kmain(void) {
  cls(DC);

  const char *bootmsg = "bados kernel";
  const int msglen = strlen(bootmsg);
  setcur(VGA_TEXT_BUF_W + (VGA_TEXT_BUF_W - msglen) / 2);
  puts(bootmsg, DC);
  setcur(VGA_TEXT_BUF_W * 3);
  put_colour_grid();
}
